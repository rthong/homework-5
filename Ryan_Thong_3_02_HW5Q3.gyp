import numpy as np
##loading txtfile
coords = np.loadtxt("random_x_y_pairs.txt")
##creating array for coords
x_coords = []
y_coords = []
##sending values into their respective arrays
for i in range(len(coords)):
    x_coords.append(coords[i][0])
    y_coords.append(coords[i][1])
##initializing va;ues
expect_value = 0
mean_y = 0
mean_x = 0
##calculating mean values for x and y coords
for i in range(len(coords)):
    expect_value += x_coords[i]*y_coords[i]
    mean_x += x_coords[i]
    mean_y += y_coords[i]

mean_x = mean_x/len(x_coords)
mean_y = mean_y/len(y_coords)
##determining numerator for formula
numerator = expect_value - (mean_x*mean_y)
##initializing standard deviations
std_x = 0
std_y = 0
##calculating standard deviations
for i in range(len(coords)):
    std_x += (x_coords[i] - mean_x)**2
    std_y += (y_coords[i] - mean_y)**2

std_x = std_x/len(x_coords)
std_y = std_y/len(y_coords)

std_x = std_x**(1/2)
std_y = std_y**(1/2)
##calculating denominator
denominator = std_x * std_y

print("The linear correlation coefficient is: ", numerator/denominator)
