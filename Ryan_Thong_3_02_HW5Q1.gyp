myArray = [2,3,4,2]

sum = 0

##creating a runnning sum
for i in range(len(myArray)):
    sum += myArray[i]
##creating mean
mean = sum/len(myArray)
##initialize standard deviation
std_dev = 0
##creating the summation part for standard deviation
for i in range(len(myArray)):
    std_dev += (mean - myArray[i])**2
##creating standard deviation
std_dev = (std_dev/len(myArray))**(1/2)

print(std_dev)