def f(x):
    return 3 * x ** 3 - 4 * x ** 2 + 3.2198 * x

a = 1
b = 9
N = [10,50,100,200]
delta_x = []

for i in range(len(N)):
    print(i)
    delta_x.append((b-a)/N[i])

x_coords_mid = [[],[],[],[]]
x_coords_right = [[], [], [], []]
x_coords_left = [[], [], [], []]

for i in range(len(N)):
    for j in range(len(N)):
        x_coords_mid[i].append( (1/2)*(a + ( (j + 1) * delta_x[i] )) )
        x_coords_left[i].append( a + (j*delta_x[i]) )
        x_coords_right[i].append( a + ((j + 1)*delta_x[i]) )

left_Riemann_sum = [[], [], [], []]
right_Riemann_sum = [[], [], [], []]
mid_sum = [[], [], [], []]

for i in range(len(N)):
    for j in range(len(N)):
        left_Riemann_sum[i] += f(x_coords_left[i])
        mid_sum[i] += f(x_coords_mid[i])
        right_Riemann_sum[i] += f(x_coords_right[i])

print(left_Riemann_sum,right_Riemann_sum,mid_sum)
